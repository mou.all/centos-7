#!/bin/bash
#
# centos-setup.sh
#
# (c) Niki Kovacs 2020 <info@microlinux.fr>

# Enterprise Linux version
VERSION="el7"

# Répertoire actuel
CWD=$(pwd)

# Utilisateurs définis
USERS="$(ls -A /home)"

# Utilisateur Admin
ADMIN=$(getent passwd 1000 | cut -d: -f 1)

# Supprimer ces packages
CRUFT=$(egrep -v '(^\#)|(^\s+$)' ${CWD}/${VERSION}/yum/useless-packages.txt)

# Installez ces packages
EXTRA=$(egrep -v '(^\#)|(^\s+$)' ${CWD}/${VERSION}/yum/extra-packages.txt)

# Defined users
USERS="$(ls -A /home)"

# Mirrors
ELREPO="https://elrepo.org/linux/elrepo/${VERSION}/x86_64/RPMS"
CISOFY="https://packages.cisofy.com"

# Log
LOG="/tmp/$(basename "${0}" .sh).log"
echo > ${LOG}

usage() {
  echo "Usage: ${0} OPTION"
  echo 'CentOS 7.x post-install configuration for servers.'
  echo 'Options:'
  echo '  -1, --shell    Configure shell: Bash, Vim, console, etc.'
  echo '  -2, --repos    Configurer des référentiels officiels et tierss.'
  echo '  -3, --extra    Installer le système de base amélioré.'
  echo '  -4, --prune    supprimer les paquets inutiles'
  echo '  -5, --logs     Autoriser le utilisateur administrateur à accéder aux journaux système.'
  echo '  -6, --ipv4     Désactivez IPv6 et reconfigurez les services de base.'
  echo '  -7, --sudo     Configurer un mot de passe persistant pour sudo.'
  echo '  -8, --setup    Effectuez tout ce qui précède en une seule fois.'
  echo '  -9, --strip    Revenir au système de base amélioré.'
  echo '  -h, --help     Afficher ce message.'
  echo "Les journaux sont écrits dans ${LOG}."
}

configure_shell() {
  # Installez des invites de commande personnalisées et une poignée d'alias astucieux.
  echo 'Configuration du shell Bash pour root.'
  cat ${CWD}/${VERSION}/bash/bashrc-root > /root/.bashrc
  echo 'Configuration du shell Bash pour les utilisateurs.'
  cat ${CWD}/${VERSION}/bash/bashrc-users > /etc/skel/.bashrc
  # Les utilisateurs existants pourraient vouloir l'utiliser.
  if [ ! -z "${USERS}" ]
  then
    for USER in ${USERS}
    do
      cat ${CWD}/${VERSION}/bash/bashrc-users > /home/${USER}/.bashrc
      chown ${USER}:${USER} /home/${USER}/.bashrc
    done
  fi
  # Ajoutez une poignée d'options astucieuses à l'échelle du système pour Vim.
  echo 'Configuration de Vim.'
  cat ${CWD}/${VERSION}/vim/vimrc > /etc/vimrc
  # Set english as main system language.
  echo 'Configuration du system locale.'
  localectl set-locale LANG=en_US.UTF8
  # Set console resolution
  if [ -f /boot/grub2/grub.cfg ]
  then
    echo 'Configuration du console resolution.'
    sed -i -e 's/rhgb quiet/nomodeset quiet vga=791/g' /etc/default/grub
    grub2-mkconfig -o /boot/grub2/grub.cfg >> ${LOG} 2>&1
  fi
}

configure_repos() {
  # Enable [base], [updates] and [extra] repos with a priority of 1.
  echo 'Configuration des référentiels de packages officiels.'
  cat ${CWD}/${VERSION}/yum/CentOS-Base.repo > /etc/yum.repos.d/CentOS-Base.repo
  sed -i -e 's/installonly_limit=5/installonly_limit=2/g' /etc/yum.conf
  # Enable [cr] repo with a priority of 1.
  echo 'Configuration du référentiel de packages CR.'
  cat ${CWD}/${VERSION}/yum/CentOS-CR.repo > /etc/yum.repos.d/CentOS-CR.repo
  # Enable [sclo] repos with a priority of 1.
  echo 'Configuration des référentiels de packages SCLo.'
  if ! rpm -q centos-release-scl > /dev/null 2>&1
  then
    yum -y install centos-release-scl >> ${LOG} 2>&1
  fi
  cat ${CWD}/${VERSION}/yum/CentOS-SCLo-scl-rh.repo > /etc/yum.repos.d/CentOS-SCLo-scl-rh.repo
  cat ${CWD}/${VERSION}/yum/CentOS-SCLo-scl.repo > /etc/yum.repos.d/CentOS-SCLo-scl.repo
  # Enable Delta RPM.
  if ! rpm -q deltarpm > /dev/null 2>&1
  then
    echo 'Activation de Delta RPM.'
    yum -y install deltarpm >> ${LOG} 2>&1
  fi
  # Initial update
  echo 'Exécution de la mise à jour initiale.'
  echo 'Cela pourrait prendre un moment...'
  yum -y update >> ${LOG} 2>&1
  # Install Yum-Priorities plugin
  if ! rpm -q yum-plugin-priorities > /dev/null 2>&1
  then
    echo 'Installer le plugin Yum-Priorities.'
    yum -y install yum-plugin-priorities >> ${LOG} 2>&1
  fi
  # Enable [epel] repo with a priority of 10.
  echo 'Configuration du référentiel de packages EPEL.' 
  if ! rpm -q epel-release > /dev/null 2>&1
  then
    yum -y install epel-release >> ${LOG} 2>&1
  fi
  cat ${CWD}/${VERSION}/yum/epel.repo > /etc/yum.repos.d/epel.repo
  cat ${CWD}/${VERSION}/yum/epel-testing.repo > /etc/yum.repos.d/epel-testing.repo
  # Configure [elrepo] and [elrepo-kernel] repos without activating them.
  echo 'Configuration des référentiels de packages ELRepo.'
  if ! rpm -q elrepo-release > /dev/null 2>&1
  then
    yum -y localinstall \
    ${ELREPO}/elrepo-release-7.0-4.${VERSION}.elrepo.noarch.rpm >> ${LOG} 2>&1
  fi
  cat ${CWD}/${VERSION}/yum/elrepo.repo > /etc/yum.repos.d/elrepo.repo
  # Enable [lynis] repo with a priority of 5.
  echo 'Configuration du référentiel de packages Lynis.'
  if [ ! -f /etc/yum.repos.d/lynis.repo ]
  then
    rpm --import ${CISOFY}/keys/cisofy-software-rpms-public.key >> ${LOG} 2>&1
  fi
  cat ${CWD}/${VERSION}/yum/lynis.repo > /etc/yum.repos.d/lynis.repo
}

install_extras() {
  echo 'Récupération des packages manquants dans le groupe de packages Core.' 
  yum group mark remove "Core" >> ${LOG} 2>&1
  yum -y group install "Core" >> ${LOG} 2>&1
  echo 'Groupe de packages principal installé sur le système.'
  echo 'Installation du groupe de packages de base.'
  echo 'Cela pourrait prendre un moment...'
  yum group mark remove "Base" >> ${LOG} 2>&1
  yum -y group install "Base" >> ${LOG} 2>&1
  echo 'Groupe de packages de base installé sur le système.'
  echo 'Installer des packages supplémentaires.'
  for PACKAGE in ${EXTRA}
  do
    if ! rpm -q ${PACKAGE} > /dev/null 2>&1
    then
      echo "Installation du package: ${PACKAGE}"
      yum -y install ${PACKAGE} >> ${LOG} 2>&1
    fi
  done
  echo 'Tous les packages supplémentaires installés sur le système.'
}

remove_cruft() {
  echo 'Suppression des packages inutiles du système.'
  for PACKAGE in ${CRUFT}
  do
    if rpm -q ${PACKAGE} > /dev/null 2>&1
    then
      echo "Suppression du package: ${PACKAGE}"
      yum -y remove ${PACKAGE} >> ${LOG} 2>&1
      if [ "${?}" -ne 0 ]
        then
        echo "Impossible de supprimer le package ${PACKAGE}." >&2
        exit 1
      fi
    fi
  done
  echo 'Tous les packages inutiles supprimés du système.'
}

configure_logs() {
  # Admin user can access system logs
  if [ ! -z "${ADMIN}" ]
  then
    if getent group systemd-journal | grep ${ADMIN} > /dev/null 2>&1
    then
      echo "l'utilisateur ${ADMIN} est déjà membre du groupe systemd-journal."
    else
      echo "Ajout de l'utilisateur admin ${ADMIN} au groupe systemd-journal."
      usermod -a -G systemd-journal ${ADMIN}
    fi
  fi
}

disable_ipv6() {
  # Désactiver IPv6
  echo 'Désactiver IPv6.'
  cat ${CWD}/${VERSION}/sysctl.d/disable-ipv6.conf > /etc/sysctl.d/disable-ipv6.conf
  sysctl -p --load /etc/sysctl.d/disable-ipv6.conf >> $LOG 2>&1
  # Reconfigurer SSH 
  if [ -f /etc/ssh/sshd_config ]
  then
    echo 'Configuration du serveur SSH pour IPv4 uniquement.'
    sed -i -e 's/#AddressFamily any/AddressFamily inet/g' /etc/ssh/sshd_config
    sed -i -e 's/#ListenAddress 0.0.0.0/ListenAddress 0.0.0.0/g' /etc/ssh/sshd_config
  fi
  # Reconfigurer Postfix
  if [ -f /etc/postfix/main.cf ]
  then
    echo 'Configuration de Postfix server pour IPv4 uniquement.'
    sed -i -e 's/inet_protocols = all/inet_protocols = ipv4/g' /etc/postfix/main.cf
    systemctl restart postfix
  fi
  # Reconstruire initrd
  echo 'Reconstruction du disque virtuel initial.'
  dracut -f -v >> $LOG 2>&1
}

configure_sudo() {
  # Configurer un mot de passe persistant pour sudo.
  if grep timestamp_timeout /etc/sudoers > /dev/null 2>&1
  then
    echo 'Mot de passe persistant pour sudo déjà configuré.'
  else
    echo 'Configuration du mot de passe persistant pour sudo.'
    echo >> /etc/sudoers
    echo '# Timeout' >> /etc/sudoers
    echo 'Defaults timestamp_timeout=-1' >> /etc/sudoers
  fi
}

strip_system() {
  # Supprimez tous les packages qui ne font pas partie du système de base amélioré.
  echo 'Stripping system.'
  local TMP='/tmp'
  local PKGLIST="${TMP}/pkglist"
  local PKGINFO="${TMP}/pkg_base"
  rpm -qa --queryformat '%{NAME}\n' | sort > ${PKGLIST}
  PACKAGES=$(egrep -v '(^\#)|(^\s+$)' $PKGLIST)
  rm -rf ${PKGLIST} ${PKGINFO}
  mkdir ${PKGINFO}
  unset REMOVE
  echo 'Creation de base de données.'
  BASE=$(egrep -v '(^\#)|(^\s+$)' ${CWD}/${VERSION}/yum/enhanced-base.txt)
  for PACKAGE in ${BASE}
  do
    touch ${PKGINFO}/${PACKAGE}
  done
  for PACKAGE in ${PACKAGES}
  do
    if [ -r ${PKGINFO}/${PACKAGE} ]
    then
      continue
    else
      REMOVE="${REMOVE} ${PACKAGE}"
    fi
  done
  if [ ! -z "${REMOVE}" ]
  then
    for PACKAGE in ${REMOVE}
    do
      if rpm -q ${PACKAGE} > /dev/null 2>&1
      then
        echo "Supression du package: ${PACKAGE}"
        yum -y remove ${PACKAGE} >> ${LOG} 2>&1
      fi
    done
  fi
  configure_repos
  install_extras
  remove_cruft
  rm -rf ${PKGLIST} ${PKGINFO}
}

# Assurez-vous que le script est exécuté avec des privilèges de superutilisateur.
if [[ "${UID}" -ne 0 ]]
then
  echo 'Veuillez exécuter avec sudo ou en tant que root.' >&2
  exit 1
fi

# Vérifier les paramètres.
if [[ "${#}" -ne 1 ]]
then
  usage
  exit 1
fi
OPTION="${1}"
case "${OPTION}" in
  -1|--shell) 
    configure_shell
    ;;
  -2|--repos) 
    configure_repos
    ;;
  -3|--extra) 
    install_extras
    ;;
  -4|--prune) 
    remove_cruft
    ;;
  -5|--logs) 
    configure_logs
    ;;
  -6|--ipv4) 
    disable_ipv6
    ;;
  -7|--sudo) 
    configure_sudo
    ;;
  -8|--setup) 
    configure_shell
    configure_repos
    install_extras
    remove_cruft
    configure_logs
    disable_ipv6
    configure_sudo
    ;;
  -9|--strip) 
    strip_system
    ;;
  -h|--help) 
    usage
    exit 0
    ;;
  ?*) 
    usage
    exit 1
esac

exit 0
